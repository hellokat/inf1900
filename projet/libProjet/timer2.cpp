#include "timer2.h"


void Timer2::setTcnt2(uint16_t valeur)
{
    TCNT2 = valeur;
}

/*
* @brief    Place les bits de TIMSK dans le mode CTC interrupt
*/
void Timer2::setTimskCtcInterrupt()
{
    TIMSK2 |= (1 << OCIE2A);
}

/*
* @brief    
*/
void Timer2::setTccr2aCtc()
{
    TCCR2A |= (1 << WGM21);
}

/*
* @brief    
*/
void Timer2::setPrescaler64()
{
    TCCR2B |= (1 << CS22);
}

/*
* @brief    
*/
void Timer2::setOcr2a()
{
    OCR2A = 100;
}

