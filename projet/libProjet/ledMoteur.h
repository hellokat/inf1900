/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Fichier .h de la classe LED qui permet de regrouper les methodes utilisees 
 * pour l´affichage des bonnes couleurs des DEL pour la roue droite et la rouge gauche
 * 
 * PIN 0 et 1 portD pour la gauche (PD0 et PD1) et PIN 2 et pour la droite (PD2 et PD7) du port D.
 * DDRD en mode sortie
 */

#ifndef F_CPU
# define F_CPU 8000000UL //8
#endif
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "fonction_utiles.h"
class LED {

    public:
    void ledVerteVerte();
    void ledVerteRouge();
    void ledRougeVerte();

};