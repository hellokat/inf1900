/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Fichier .h de la classe Capteur qui permet de regrouper les methodes utilisees 
 * pour avoir la distance entre un objet et un cpateur pour chaque capteur
 * PIN 0 a 3 du port A, A0 est en mode sortie , A1 a A3 en mode entrée et le reste en sortie
 */ 
#ifndef F_CPU
#define F_CPU 8000000UL //8 MHz
#endif
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "Timer0.h"
#include "moteur.h"
class Capteur {
public:
   Capteur();
   ~Capteur();
   Capteur(const uint8_t&);
   void assignerCapteur();
   EtatCapteur getEtatCapteur();
   void setDistance(float);
   double getDistance();
   Distance getDistanceString();
   void lancerTimer0();
   void calculerDistance();
   void initialiserCapteur();
   void setValeurMasque(const uint8_t);
   
private:
   float distance_;
   EtatCapteur etatCapteur_;
   uint8_t valeurMasque_;
};