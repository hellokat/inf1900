/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Code qui n'est sous aucune license.
 *
 */

#ifndef F_CPU
#define F_CPU 8000000UL //8 MHz
#endif
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>

class Timer2 
{
public:
    Timer2() = default;
    void setTcnt2(uint16_t valeur);
    void setTimskCtcInterrupt();
    void setTccr2aCtc();
    void setPrescaler64();
    void setOcr2a();
};