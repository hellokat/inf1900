/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Fichier .h de la classe Robot qui permet de regrouper les methodes utilisees pour passer de mode detection a mode manoeuvre
 *
 */

#include "Affichage.h"



class Robot {
public:
   Robot();
   ~Robot();
   void modeDetection(LCM&);
   void modeManoeuvre(LCM&);
   void trouverManoeuvre();
   void calculerDistances();
   void assignerCapteurs();
   Capteur& getCapteurGauche();
   Capteur& getCapteurMilieu();
   Capteur& getCapteurDroite();
   Moteur& getMoteur();
   void setMode(const Mode&);
   Mode getModeRobot();

private:
   Capteur capteurGauche_;
   Capteur capteurMilieu_;
   Capteur capteurDroite_;
   Mode mode_;
   Moteur moteur_;
   Manoeuvre manoeuvre_;
   Affichage affichage_;
};