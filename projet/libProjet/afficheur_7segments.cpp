#include "afficheur_7segments.h"

SeptSegments::SeptSegments() {
    position_ = caseUn;
}

void SeptSegments::commencerTimer2() {
    cli();
    Timer2 timer2;
    timer2.setOcr2a();
    timer2.setPrescaler64();
    timer2.setTccr2aCtc();
    timer2.setTcnt2(0);
    timer2.setTimskCtcInterrupt();
    sei();
}

void SeptSegments::arreterTimer2() {
    TCCR2B &= ~(1 << CS22);
}

void SeptSegments::afficherPuissance (int pourcentageGauche, int pourcentageDroite) {
        switch (position_) {

        case caseUn :
        PORTA = 0xf8;
        PORTA = position_;
        PORTC = chiffres_[pourcentageGauche / 10];
        position_ = caseDeux;
        break;
               
        case caseDeux :
        PORTA = 0xf8;
        PORTA = position_;
        PORTC = chiffres_[pourcentageGauche % 10];
        position_ = caseTrois;
        break;
        
        case caseTrois :
        PORTA = 0xf8;
        PORTA = position_;
        PORTC = chiffres_[10];
        position_ = caseQuatre;
        break;
        
        case caseQuatre :
        PORTA = 0xf8;
        PORTA = position_;
        PORTC = chiffres_[pourcentageDroite / 10];
        position_ = caseCinq;
        break;
        
        case caseCinq :
        PORTA = 0xf8;
        PORTA = position_;
        PORTC = chiffres_[pourcentageDroite % 10];
        position_ = caseUn;
        break;
    }
}

/*
void SeptSegments::afficher7Segments(int pourcentageGauche, int pourcentageDroite) {
    commencerTimer2();
    afficherPuissance(pourcentageGauche, pourcentageDroite);
}
*/