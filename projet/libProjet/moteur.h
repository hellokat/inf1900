/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Fichier .h de la classe Moteur qui permet de regrouper les methodes utilisees pour modifier 
 * le pwm du moteur de la roue droite et de la roue gauche pour executer le mode manoeuvre
 * 
 * PIN 4 a pour la roue gauche et PIN 5 pour la roue droite du port D
 * DDRB en mode sortie
 
 */
#ifndef F_CPU
#define F_CPU 8000000UL //8 MHz
#endif
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "timer1.h"
#include "afficheur_7segments.h"
#include "ledMoteur.h"



class Moteur{
    public:

    Moteur();
    ~Moteur();

    void setRegistres();
    void demarrerMoteur();
    void arreterMoteur();
    void setPourcentagePWMDroit(int );
    void setPourcentagePWMGauche(int );
    int getPourcentageDroit();
    int getPourcentageGauche();
    void diminuerVitesseGauche(int, int );
    void diminuerVitesseDroite(int, int );
    void augmenterVitesseGauche(int, int );
    void augmenterVitesseDroite(int, int );
    void vitesseCourante(int, int );
    void modeM1();
    void modeM2();
    void modeM3();
    void modeM4();
    void modeM5();
    void modeM6();
    void executerManoeuvre(Manoeuvre);
   
    private:
    SeptSegments septSegments_;
    LED led_;
    int pwmDroit_; 
    int pwmGauche_; 
};