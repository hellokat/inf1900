/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Fichier cpp de la classe Timer1 comportant l’implémentation des méthodes de celle-ci 
 * 
 */

#include "timer1.h"
Timer1::Timer1(){}
/*
* @brief    Affecte le paramètre valeur à la valeur de TCNT1
* @param    valeur     l'entier à affecter
*/
void Timer1::setTcnt1(uint16_t valeur)
{
    TCNT1 = valeur;
}

/*
* @brief    Place les bits de TIMSK à 1 en fonction des paramètres
* @param    lettre1    caractère choisissant si le A est activé
* @param    lettre2    caractère choisissant si le B est activé
* @param    lettre3    caractère choisissant si l'Overflow est activé
*/
void Timer1::setTimsk(char lettre1,char lettre2, char lettre3)
{
    if (lettre1 == 'a')
    {
        TIMSK1 |= (1 << OCIE1A);
       
    }
    if (lettre2 == 'b')
    {
        TIMSK1 |= (1 << OCIE1B);
        
    }
    if (lettre3 == 'o')
    {
        TIMSK1 |= (1 << TOIE1);
       
    }
}

/*
* @brief    Affecte le paramètre valeur à la valeur de OICR1A
* @param    valeur     l'entier à affecter
*/
void Timer1::setOicr1a(uint16_t valeur)
{
    OCR1A = valeur;
 
}

/*
* @brief    Affecte le paramètre valeur à la valeur de OICR1B
* @param    valeur     l'entier à affecter
*/
void Timer1::setOicr1b(uint16_t valeur)
{
    OCR1B = valeur;
  
}

/*
* @brief    Place les bits de TCCR1A à 1 en fonction des paramètres
* @param    wgm10   valeur à affecter à WGM10  
* @param    wgm11   valeur à affecter à WGM11
* @param    wgm12   valeur à affecter à WGM12
* @param    wgm13   valeur à affecter à WGM13
* @param    com1a0  valeur à affecter à COM1A0
* @param    com1a1  valeur à affecter à COM1A1
* @param    com1b0  valeur à affecter à COM1B0
* @param    com1b1  valeur à affecter à COM1B1
*/
void Timer1::setTccr1a(uint8_t wgm10,uint8_t wgm11,uint8_t wgm12,uint8_t wgm13,uint8_t com1a0,uint8_t com1a1,uint8_t com1b0,uint8_t com1b1)
{
    TCCR1A |= (wgm10 << WGM10);
    TCCR1A |= (wgm11 << WGM11);
    TCCR1A |= (wgm12 << WGM12);
    TCCR1A |= (wgm13 << WGM13);
    TCCR1A |= (com1a0 << COM1A0);
    TCCR1A |= (com1a1 << COM1A1);
    TCCR1A |= (com1b0 << COM1B0);
    TCCR1A |= (com1b1 << COM1B1);
    

}

/*
* @brief    Place les bits de TCCR1B à 1 en fonction des paramètres
* @param    cs10   valeur à affecter à CS10  
* @param    cs11   valeur à affecter à CS11
* @param    cs12   valeur à affecter à CS12
*/
void Timer1::setTccr1b(uint8_t cs10,uint8_t cs11,uint8_t cs12)
{
    TCCR1B |= (cs10 << CS10); 
    TCCR1B |= (cs11 << CS11);
    TCCR1B |= (cs12 << CS12);

}

/*
* @brief    Affecte le paramètre valeur à la valeur de TCCR1C
* @param    valeur     l'entier à affecter
*/
void Timer1::setTccr1c(uint8_t valeur)
{
    TCCR1C = valeur;

}

/*
* @brief    Place le timer1 dans le mode CTC set on compare match A
*/
void Timer1::setModeCTCsetOnCompareMatchA()
{
    setTccr1a(0,0,1,0,1,1,0,0);
}

/*
* @brief    Place le timer1 dans le mode CTC clear on compare match A
*/
void Timer1::setModeCTCclearOnCompareMatchA()
{
    setTccr1a(0,0,1,0,0,1,0,0);
}

/*
* @brief    Place le timer1 dans le mode CTC toggle on compare match A
*/
void Timer1::setModeCTCtoggleOnCompareMatchA()
{
    setTccr1a(0,0,1,0,1,0,0,0);
}

/*
* @brief    Place le timer1 dans le mode Phase Correct clear when upcounting A
*/
void Timer1::setModePhaseCorrectClearUpcountingA()
{
    setTccr1a(1,0,0,0,0,1,0,0);
}

/*
* @brief    Place le timer1 dans le mode Phase Correct set when upcounting A
*/
void Timer1::setModePhaseCorrectSetUpcountingA()
{
    setTccr1a(1,0,0,0,1,1,0,0);
}

/*
* @brief    Place le timer1 dans le mode CTC set on compare match B
*/
void Timer1::setModeCTCsetOnCompareMatchB()
{
    setTccr1a(0,0,1,0,0,0,1,1);
}

/*
* @brief    Place le timer1 dans le mode CTC clear on compare match B
*/
void Timer1::setModeCTCclearOnCompareMatchB()
{
    setTccr1a(0,0,1,0,0,0,0,1);
}

/*
* @brief    Place le timer1 dans le mode CTC toggle on compare match B
*/
void Timer1::setModeCTCtoggleOnCompareMatchB()
{
    setTccr1a(0,0,1,0,0,0,1,0);
}

/*
* @brief    Place le timer1 dans le mode Phase Correct clear when upcounting B
*/
void Timer1::setModePhaseCorrectClearUpcountingB()
{
    setTccr1a(1,0,0,0,0,0,0,1);
}

/*
* @brief    Place le timer1 dans le mode Phase Correct set when upcounting B
*/
void Timer1::setModePhaseCorrectSetUpcountingB()
{
    setTccr1a(1,0,0,0,0,0,1,1);
}

/*
* @brief    Place le timer1 dans le mode Phase Correct clear when upcounting AB
*/
void Timer1::setModePhaseCorrectClearUpcountingAB()
{
    setTccr1a(1,0,0,0,0,1,0,1);
}

/*
* @brief    Met un prescaler de 1 dans le timer 1
*/
void Timer1::setPrescaler1()
{
    setTccr1b(1,0,0);
}

/*
* @brief    Met un prescaler de 8 dans le timer 1
*/
void Timer1::setPrescaler8()
{
    setTccr1b(0,1,0);
}

/*
* @brief    Met un prescaler de 64 dans le timer 1
*/
void Timer1::setPrescaler64()
{
    setTccr1b(1,1,0);
}

/*
* @brief    Met un prescaler de 256 dans le timer 1
*/
void Timer1::setPrescaler256()
{
    setTccr1b(0,0,1);
}

/*
* @brief    Met un prescaler de 1024 dans le timer 1
*/
void Timer1::setPrescaler1024()
{
    setTccr1b(1,0,1);
}


