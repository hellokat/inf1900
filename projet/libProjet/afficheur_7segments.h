#ifndef F_CPU
#define F_CPU 8000000UL //8 MHz
#endif
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "timer2.h"

	
enum Position { caseUn = 0xf7 , caseDeux = 0xef , caseTrois = 0xdf, caseQuatre = 0xbf, caseCinq= 0x7f};

class SeptSegments {
public: 
    SeptSegments();
    SeptSegments(Position position_);
    void afficherPuissance(int pourcentageGauche, int pourcentageDroite);
    void commencerTimer2(); 
    void arreterTimer2();
    void afficher7Segments(int pourcentageGauche, int pourcentageDroite);

private:

    Position position_;

    const uint8_t chiffres_[11] = {0xfc, 0x60, 0xda, 0xf2, 0x66, 0xb6, 0xbe, 0xe0, 0xfe, 0xf6, 0x02}; // hex value of 0-9 and '-'
};

