/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Code qui n'est sous aucune license.
 *
 */

#ifndef F_CPU
# define F_CPU 8000000UL //8
#endif
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>


//define a la place??
const int FACTEUR_US_VERS_METRE = 5800;
const float VALEUR_MAX_8BITS = 255;
const int VALEUR_MAX_DNGR = 1;
const int VALEUR_MIN_OK = 3;
const int FACTEUR_DEPLACEMENT_VIRGULE_VERS_GAUCHE = 10;
const int MODE_ENTREE = 0x00;
const int MODE_SORTIE = 0xFF;
const int COULEUR_ROUGE = 0x02;
const int COULEUR_VERTE = 0x01;

/*
* @brief    Utilisee dans Capteur.cpp pour avoir 1 chiffre avant la virgule 
*           et 1 apres la virgule pour la distance captee par le Sonor
*/
struct Distance {int premierChiffre;int deuxiemeChiffre;};

/*
* @brief    Fonction qui execute la couleur jaune
*/
void couleurJaune();

/*
* @brief    Enum pour identifier si le robot est en mode detection ou mode manoeuvre
*/
enum Mode {MODE_DETECTION, MODE_MANOEUVRE};

/*
* @brief    Enum pour identifier quelle manuevre est executee 
*/
enum Manoeuvre {DEFAUT, M1, M2, M3, M4, M5, M6};

/*
* @brief    Enum pour identifier les 3 etats que peut avoir le capteur
*/
enum EtatCapteur {OK, ATTN, DNGR};

/*
* @brief    Enum pour savoir si le bouton etait appuye ou relache
*/
enum Bouton_etait{APPUYER = 1, LAISSER = 0};

/*
* @brief    Enum pour verifier si le bouton est pese ou relache
*/
enum Bouton_est{PESER = 1, RELACHER = 0};

/*
* @brief    Fonction qui verifie si le bouton est pese
*/
bool boutonEst();
