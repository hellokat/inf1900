/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Fichier cpp de la classe Moteur comportant l’implémentation des méthodes de celle-ci 
 * 
 * PIN 4 a pour la roue gauche et PIN 5 pour la roue droite du port D
 * DDRB en mode sortie
 */


#include "moteur.h"


/*
* @brief    Constructeur par defaut qui initialise les attributs pwmDroit_ et pwmGauche_ a 0   
*/
Moteur::Moteur(): pwmDroit_(0), pwmGauche_(0){}

/*
* @brief    Destructeur par defaut
*/
Moteur::~Moteur(){}


/*
* @brief    Methode qui permet de demarrer les moteurs de la roue droite et de gauche    
*/
void Moteur::demarrerMoteur()
{
    Timer1 timer;
    timer.setModePhaseCorrectClearUpcountingAB(); //tccr1a phase correct upcounting A et B
    timer.setPrescaler8(); //tccr1b pour prescaler 
    timer.setTccr1c(0);     //tccr1c a 0
    timer.setTcnt1(0);      //compteur a 0
 
}

/*
* @brief    Methode qui permet d´arreter le moteur de la roue droite et de la gauche    
*/
void Moteur::arreterMoteur()
{
    Timer1 timer;
    timer.setTccr1a(0,0,0,0,0,0,0,0);
    timer.setTccr1b(0,0,0);
    timer.setTccr1c(0);
    OCR1A=0;
    OCR1B=0;
    pwmDroit_ = 0;
    pwmGauche_ = 0;

}


/*
* @brief    Calculer le pourcentage pwm que nous allons utiliser pour afficher sur l´afficheur sept segments pour la roue droite
*           dont le resultat a ete affecte dans l´attribut pwmDroit_
* @param    vitesseDroite     Vitesse pour laquelle on veut calculer le pourcentage pwm
*/
void Moteur::setPourcentagePWMDroit(int vitesseDroite)
{
    
    pwmDroit_ = (((255 - vitesseDroite)/255.0)*100);


}

/*
* @brief    Calculer le pourcentage pwm que nous allons utiliser pour afficher sur l´afficheur sept segments pour la roue gauche
*           dont le resultat a ete affecte dans l´attribut pwmGauche_
* @param    vitesseGauche     Vitesse pour laquelle on veut calculer le pourcentage pwm
*/
void Moteur::setPourcentagePWMGauche(int vitesseGauche)
{
    
    pwmGauche_ = (((255 - vitesseGauche)/255.0)*100);

}

int Moteur::getPourcentageDroit()
{
    return pwmDroit_;
}

int Moteur::getPourcentageGauche()
{
    return pwmGauche_;
}

/*
* @brief    Boucle qui permet de diminuer la vitesse de la roue gauche
* @param    vitesseInitialeGauche       Vitesse que nous voulons diminuer pour la roue gauche
* @param    vitesseFinale               Vitesse finale que nous voulons que la vitesse initiale atteint
*/
void Moteur::diminuerVitesseGauche(int vitesseInitialeGauche, int vitesseFinale)
{
    for(int i=vitesseInitialeGauche; i>= vitesseFinale; i--)
    {
    
        _delay_ms(100);		
        OCR1B=i;
        setPourcentagePWMGauche(i);
    }
}

/*
* @brief    Boucle qui permet de diminuer la vitesse de la roue droite
* @param    vitesseInitialeDroite       Vitesse que nous voulons diminuer pour la roue droite
* @param    vitesseFinale               Vitesse finale que nous voulons que la vitesse initiale atteint
*/
void Moteur::diminuerVitesseDroite(int vitesseInitialeDroite, int vitesseFinale)
{
    for(int i=vitesseInitialeDroite; i>= vitesseFinale; i--)
    {
        _delay_ms(100);		
        OCR1A=i;
        setPourcentagePWMDroit(i);
    }
            
}

/*
* @brief    Boucle qui permet d´augmenter la vitesse de la roue gauche
* @param    vitesseInitialeGauche       Vitesse que nous voulons augmenter pour la roue gauche
* @param    vitesseFinale               Vitesse finale que nous voulons que la vitesse initiale atteint
*/
void Moteur::augmenterVitesseGauche(int vitesseInitialeGauche, int vitesseFinale)
{
    for(int i=vitesseInitialeGauche; i<= vitesseFinale; i++)
    {
        _delay_ms(100);
        OCR1B=i;
        setPourcentagePWMGauche(i);
    }
}

/*
* @brief    Boucle qui permet d´augmenter la vitesse de la roue droite
* @param    vitesseInitialeDroite       Vitesse que nous voulons augmenter pour la roue droite
* @param    vitesseFinale               Vitesse finale que nous voulons que la vitesse initiale atteint
*/
void Moteur::augmenterVitesseDroite(int vitesseInitialeDroite, int vitesseFinale)
{
    for(int i=vitesseInitialeDroite; i<= vitesseFinale; i++)
    {
        _delay_ms(100);
        OCR1A=i;
        setPourcentagePWMDroit(i);
    }
}

/*
* @brief    Permet de fixe les registres de la roue droite et de la gauche a une vitesse constante/ fixe
* @param    vitesseGauche       Vitesse desiree de la roue gauche 
* @param    vitesseDroite       Vitesse desiree de la roue droite
*/
void Moteur::vitesseCourante(int vitesseGauche, int vitesseDroite)
{
    OCR1A= vitesseDroite;
    OCR1B= vitesseGauche;
    setPourcentagePWMGauche(vitesseGauche);
    setPourcentagePWMDroit(vitesseDroite);
}


/*
* @brief    Executer le mode manoeuvre 1 apres le mode detection
*/
void Moteur::modeM1()
{
    setPourcentagePWMDroit(230);
    led_.ledRougeVerte();

    OCR1A= 90;                      //vitesse droite
    diminuerVitesseGauche(90, 52);
    _delay_ms(1000);
    augmenterVitesseGauche(52, 90);
    _delay_ms(2000);
    
}

/*
* @brief    Executer le mode manoeuvre 2 apres le mode detection
*/
void Moteur::modeM2()
{
    setPourcentagePWMGauche(230);
    led_.ledVerteVerte();

    OCR1B=90;                       //vitesse gauche
    diminuerVitesseDroite(90, 52);
    _delay_ms(1000);
    augmenterVitesseDroite(52, 90);
    _delay_ms(2000);
}

/*
* @brief    Executer le mode manoeuvre 3 apres le mode detection
*/
void Moteur::modeM3()
{
    led_.ledRougeVerte();		
    vitesseCourante(50, 50);
    _delay_ms(1000);

    led_.ledVerteVerte();	
    vitesseCourante(66, 66);
    _delay_ms(2000);


    led_.ledVerteRouge();		
    vitesseCourante(50, 50);
    _delay_ms(1000);

    led_.ledVerteVerte();		
    vitesseCourante(78, 78);
    _delay_ms(2000);

}

/*
* @brief    Executer le mode manoeuvre 4 apres le mode detection
*/
void Moteur::modeM4()
{
    led_.ledVerteRouge();
    vitesseCourante(50, 50);
    _delay_ms(1000);

    led_.ledVerteVerte();
    vitesseCourante(66, 66);
    _delay_ms(2000);

    led_.ledRougeVerte();
    vitesseCourante(50, 50);
    _delay_ms(1000);

    led_.ledVerteVerte();
    vitesseCourante(78, 78);
    _delay_ms(2000);
}

/*
* @brief    Executer le mode manoeuvre 5 apres le mode detection
*/
void Moteur::modeM5()
{
    led_.ledVerteRouge();
    vitesseCourante(50, 50);
    _delay_ms(2000);

    led_.ledVerteVerte();
    vitesseCourante(0, 0);	

    for(int i =0; i<= 63; i=i+3)
    {
        _delay_ms(125);
        OCR1A=i;                        //vitesse droite
        OCR1B=i;                        //vitesse gauche
        setPourcentagePWMGauche(i);
        setPourcentagePWMDroit(i);
    }
    _delay_ms(2000);
}

/*
* @brief    Executer le mode manoeuvre 6 apres le mode detection
*/
void Moteur::modeM6()
{
    led_.ledVerteVerte();
    for(int i=90; i>= 41; i=i-7)
    {
        _delay_ms(500);	
        OCR1A=i;	                    //vitesse droite
        OCR1B=i;                        //vitesse gauche
        setPourcentagePWMGauche(i);
        setPourcentagePWMDroit(i);
        
    }
    _delay_ms(2000);
}



/*
* @brief    Executer le bon mode manoeuvre selon le resultat du mode detection
* @param    manoeuvre       L´enum Manoeuvre qui permet de trouver le bon mode manoeuvre avec la fonction trouverManoeuvre
*/
void Moteur::executerManoeuvre(Manoeuvre manoeuvre) 
{
    
    switch (manoeuvre)
    {
        case M1:
            demarrerMoteur();
            modeM1();
            _delay_us(60);
            arreterMoteur();          
            break;

        case M2:
            demarrerMoteur();
            modeM2();
            _delay_us(60);
            arreterMoteur();
            break;

        case M3:
            demarrerMoteur();
            modeM3();
            _delay_us(60);
            arreterMoteur();
            break;

        case M4:
            demarrerMoteur();
            modeM4();
            _delay_us(60);
            arreterMoteur();
            break;

        case M5:
            demarrerMoteur();
            modeM5();
            _delay_us(60);
            arreterMoteur();
            break;

        case M6:
            demarrerMoteur();
            modeM6();
            _delay_us(60);
            arreterMoteur();
            break;

        case DEFAUT: 
            arreterMoteur();
            break;
        
    }
}