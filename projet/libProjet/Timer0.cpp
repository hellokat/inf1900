/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Fichier cpp de la classe Timer0 comportant l’implémentation des méthodes de celle-ci 
 * 
 */

#include "Timer0.h"
Timer0::Timer0(){}
/*
* @brief    Affecte le paramètre valeur à la valeur de TCNT0
* @param    valeur     l'entier à affecter
*/
void Timer0::setTcnt0(uint16_t valeur)
{
    TCNT0 = valeur;
}

/*
* @brief    Place les bits de TIMSK à 1 en fonction des paramètres
* @param    lettre1    caractère choisissant si le A est activé
* @param    lettre2    caractère choisissant si le B est activé
* @param    lettre3    caractère choisissant si l'Overflow est activé
*/
void Timer0::setTimsk(char lettre1,char lettre2, char lettre3)
{
    if (lettre1 == 'a')
    {
        TIMSK0 |= (1 << OCIE0A);
    }
    if (lettre2 == 'b')
    {
        TIMSK0 |= (1 << OCIE0B);
    }
    if (lettre3 == 'o')
    {
        TIMSK0 |= (1 << TOIE0);
    }
}

/*
* @brief    Affecte le paramètre valeur à la valeur de OICR0A
* @param    valeur     l'entier à affecter
*/
void Timer0::setOicr0a(uint16_t valeur)
{
    OCR0A = valeur;

}

/*
* @brief    Affecte le paramètre valeur à la valeur de OICR0B
* @param    valeur     l'entier à affecter
*/
void Timer0::setOicr0b(uint16_t valeur)
{
    OCR0B = valeur;
}

/*
* @brief    Place les bits de TCCR0A à 1 en fonction des paramètres
* @param    wgm00   valeur à affecter à WGM00  
* @param    wgm01   valeur à affecter à WGM01
* @param    wgm02   valeur à affecter à WGM02
* @param    com0a0  valeur à affecter à COM0A0
* @param    com0a1  valeur à affecter à COM0A1
* @param    com0b0  valeur à affecter à COM0B0
* @param    com0b1  valeur à affecter à COM0B1
*/
void Timer0::setTccr0a(uint8_t wgm00,uint8_t wgm01,uint8_t wgm02,uint8_t com0a0,uint8_t com0a1,uint8_t com0b0,uint8_t com0b1)
{
    TCCR0A |= (wgm00 << WGM00);
    TCCR0A |= (wgm01 << WGM01);
    TCCR0A |= (wgm02 << WGM02);
    TCCR0A |= (com0a0 << COM0A0);
    TCCR0A |= (com0a1 << COM0A1);
    TCCR0A |= (com0b0 << COM0B0);
    TCCR0A |= (com0b1 << COM0B1);

}

/*
* @brief    Place les bits de TCCR0B à 1 en fonction des paramètres
* @param    cs00   valeur à affecter à CS00  
* @param    cs01   valeur à affecter à CS01
* @param    cs02   valeur à affecter à CS02
*/
void Timer0::setTccr0b(uint8_t cs00,uint8_t cs01,uint8_t cs02)
{
    TCCR0B |= (cs00 << CS00); 
    TCCR0B |= (cs01 << CS01);
    TCCR0B |= (cs02 << CS02);

}


void Timer0::setNormalModeOverflow()
{
    setTccr0a(0,0,0,1,1,0,0);
}

/*
* @brief    Place le timer0 dans le mode CTC set on compare match A
*/

void Timer0::setModeCTCsetOnCompareMatchA()
{
    setTccr0a(0,1,0,1,1,0,0);
}

/*
* @brief    Place le timer0 dans le mode CTC clear on compare match A
*/
void Timer0::setModeCTCclearOnCompareMatchA()
{
    setTccr0a(0,1,0,0,1,0,0);
}

/*
* @brief    Place le timer0 dans le mode CTC toggle on compare match A
*/
void Timer0::setModeCTCtoggleOnCompareMatchA()
{
    setTccr0a(0,1,0,1,0,0,0);
}

/*
* @brief    Place le timer0 dans le mode CTC set on compare match B
*/
void Timer0::setModeCTCsetOnCompareMatchB()
{
    setTccr0a(0,1,0,0,0,1,1);
}

/*
* @brief    Place le timer0 dans le mode CTC clear on compare match B
*/
void Timer0::setModeCTCclearOnCompareMatchB()
{
    setTccr0a(0,1,0,0,0,0,1);
}

/*
* @brief    Place le timer0 dans le mode CTC toggle on compare match B
*/
void Timer0::setModeCTCtoggleOnCompareMatchB()
{
    setTccr0a(0,1,0,0,0,1,0);
}

/*
* @brief    Met un prescaler de 1 dans le timer 1
*/
void Timer0::setPrescaler1()
{
    setTccr0b(1,0,0);
}

/*
* @brief    Met un prescaler de 1 dans le timer 1
*/
void Timer0::setPrescaler8()
{
    setTccr0b(0,1,0);
}

/*
* @brief    Met un prescaler de 1 dans le timer 1
*/
void Timer0::setPrescaler64()
{
    setTccr0b(1,1,0);
}

/*
* @brief    Met un prescaler de 1 dans le timer 1
*/
void Timer0::setPrescaler256()
{
    setTccr0b(0,0,1);
}

/*
* @brief    Met un prescaler de 1 dans le timer 1
*/
void Timer0::setPrescaler1024()
{
    setTccr0b(1,0,1);
}


