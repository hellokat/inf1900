/*
 * Ecole Polytechnique de Montreal
 * Departement de genie informatique
 * INF1900
 *
 * Morgan De Gregorio Beaudoin, Alison Nemr, Jonathan Siclait, Katherine Sheridan
 * Vendredi 17 avril 2020
 *
 * Code qui n'est sous aucune license.
 *
 */
#ifndef F_CPU
#define F_CPU 8000000UL //8 MHz
#endif
#include <Robot.h>
#define MASQUEGAUCHE 0x01
#define MASQUEMILIEU 0x02
#define MASQUEDROITE 0x04

// Global object
SeptSegments septSegments;
Robot robot;
volatile bool BOUTON_APPUIE = false;

//Fonction dinterruption pour le bouton
ISR(INT1_vect)
{
    BOUTON_APPUIE = true;
}

//Fonction dinterruption 7seg
ISR (TIMER2_COMPA_vect) {
septSegments.afficherPuissance(robot.getMoteur().getPourcentageGauche(), robot.getMoteur().getPourcentageDroit()); 
}

//Fonction qui initialise le bouton
void initBouton()
{
    cli();
    EIMSK |= (1<<INT1);
    EICRA |= (1<<ISC10)|(1<<ISC11);
    sei();   
}

int main()
{
    bool estEnMarche = true;
    initBouton();
    DDRA = 0xF8;
    DDRB = MODE_SORTIE;
    DDRC = MODE_SORTIE;
    DDRD = 0xF7;
    LCM ecran(&DDRB,&PORTB);
    robot.getCapteurGauche().setValeurMasque(MASQUEGAUCHE);
    robot.getCapteurMilieu().setValeurMasque(MASQUEMILIEU);
    robot.getCapteurDroite().setValeurMasque(MASQUEDROITE);

    while(estEnMarche)
    {
        //septSegments.afficherPuissance(45, 67);
        //robot.getMoteur().setPourcentagePWMGauche(45);
        //robot.getMoteur().setPourcentagePWMDroit(67);
        //septSegments.afficherPuissance(robot.getMoteur().getPourcentageGauche(), robot.getMoteur().getPourcentageDroit());
       if(robot.getModeRobot() == MODE_DETECTION){
           robot.modeDetection(ecran);
       }
       else{
            septSegments.commencerTimer2();
            robot.modeManoeuvre(ecran);
            septSegments.arreterTimer2();
            PORTA = 0xf8;
           
       }
       if(BOUTON_APPUIE) {
           robot.setMode(MODE_MANOEUVRE);
           BOUTON_APPUIE = false;
        }
    }
    
}